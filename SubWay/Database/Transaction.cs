﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace SubWay.Database
{
     public class Transaction
    {
        [PrimaryKey, AutoIncrement]

        public int Id { get; set; }

        [NotNull]
        [MaxLength(200)]
        public string name { get; set; }
        public int count { get; set; }
    }
}
