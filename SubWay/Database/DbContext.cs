﻿using SQLite;
using SubWay.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace SubWay.Database
{
    public class DbContext
    {
        private SQLiteConnection db;
        private string dbPath;
        public DbContext()
        {
            dbPath = Xamarin.Forms.DependencyService.Get<IFileHelper>().GetDbFilePath("subway.db");
            db = new SQLiteConnection(dbPath);
            db.CreateTable<Transaction>();
        }
        public IEnumerable<Transaction> GetAllTransaction()
        {
            return db.Table<Transaction>().ToList();
        }

        public Transaction GetTransactionId(int Id)
        {
            return db.Table<Transaction>().FirstOrDefault(t => t.Id == Id);
        }
        public void AddOrEditTransaction(Transaction transaction)
        {
            if (transaction.Id == 0)
            {
                db.Insert(transaction);
            }
            else 
            {
                db.Update(transaction);
            }
        }
        public void DeleteTransaction(Transaction transaction)
        {
            db.Delete(transaction);
        }
    }
}
