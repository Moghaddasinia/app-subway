﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SubWay.Helpers
{
   public interface IFileHelper
    {
         string GetDbFilePath(string dbname);
    }
}
