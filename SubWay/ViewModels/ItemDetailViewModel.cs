﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows.Input;
using SubWay.Database;
using SubWay.Models;
using SubWay.Services;
using Xamarin.Forms;

namespace SubWay.ViewModels
{
    [QueryProperty(nameof(ItemId), nameof(ItemId))]
    public class ItemDetailViewModel : BaseViewModel
    {
        private string itemId;
        private string text;
        private string description;
        public Command LoadItemsCommand { get; }
        public string Id { get; set; }
        public ObservableCollection<Items> Foods { get; }

        public ItemDetailViewModel()
            {
         

            Foods = new ObservableCollection<Items>();
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommandItems());

            ClickCommand = new Command(async (arg) => {
                Console.WriteLine(arg);

            });


        }
        public ICommand ClickCommand { get; set; }
        async Task ExecuteLoadItemsCommandItems()
        {
            IsBusy = true;

            try
            {
                Foods.Clear();
                var employes = await InfoService.GetAllData();
                foreach (var item in employes.ites)
                {
                    Foods.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        public string Text
        {
            get => text;
            set => SetProperty(ref text, value);
        }

        public string Description
        {
            get => description;
            set => SetProperty(ref description, value);
        }
       
        public string ItemId
        {
            get
            {
                return itemId;
            }
            set
            {
                itemId = value;
                LoadItemId(value);
            }
        }

      

        public async void LoadItemId(string itemId)
        {
            try
            {
                var item = await DataStore.GetItemAsync(itemId);
                Id = item.Id;
                Text = item.Text;
                Description = item.Description;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
    }
}
