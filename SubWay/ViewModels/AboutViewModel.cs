﻿using SubWay.Models;
using SubWay.Services;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace SubWay.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        public AboutViewModel()
        {
            Title = "About";
            LoadInfoCompany();
              OpenWebCommand = new Command(async () => await Browser.OpenAsync("https://aka.ms/xamain-quickstart"));
        }

        public ObservableCollection<Employe> Employe { get; }
      
        public ICommand OpenWebCommand { get; }
        public Employe BindingContext { get; private set; }
        public string Name { get; private set; }
        public string Address { get; private set; }
        public string Owner { get; private set; }

        public async void OnAppearing()
        {
            var employes = await InfoService.GetAllData();
            BindingContext = employes;
        }

        public async void LoadInfoCompany()
        {
            try
            {
                var employes = await InfoService.GetAllData();
                Name = employes.name;
                Address = employes.address;
                Owner = employes.owner;
            }
            catch (Exception)
            {
                Debug.WriteLine("Failed to Load Item");
            }
        }
    }
}