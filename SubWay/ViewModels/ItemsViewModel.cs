﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Threading.Tasks;

using Xamarin.Forms;

using SubWay.Models;
using SubWay.Views;
using SubWay.Services;

namespace SubWay.ViewModels
{
    public class ItemsViewModel : BaseViewModel
    {
        private Item _selectedItem;

        public ObservableCollection<Item> Items { get; }
        public ObservableCollection<EmployEnd> EmployEnds { get; }
        public Command LoadItemsCommand { get; }
        public Command AddItemCommand { get; }
        public Command<EmployEnd> ItemTapped { get; }

        public ItemsViewModel()
        {
            Title = "Employees";
            Items = new ObservableCollection<Item>();
            EmployEnds = new ObservableCollection<EmployEnd>();
            
            //LoadItemsCommand11 = new Command(async () => await ExecuteLoadItemsCommand());
            LoadItemsCommand = new Command(async () => await ExecuteLoadItemsCommandEmployye());

            ItemTapped = new Command<EmployEnd>(OnItemSelected);

            AddItemCommand = new Command(OnAddItem);
        }

        async Task ExecuteLoadItemsCommand()
        {
            IsBusy = true;

            try
            {
                Items.Clear();
                var items = await DataStore.GetItemsAsync(true);
                foreach (var item in items)
                {
                    Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }
        async Task ExecuteLoadItemsCommandEmployye()
        {
            IsBusy = true;

            try
            {
                EmployEnds.Clear();
                var employes = await InfoService.GetAllData();
                foreach (var item in employes.emloyees)
                {
                    EmployEnds.Add(item);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
            finally
            {
                IsBusy = false;
            }
        }

        public void OnAppearing()
        {
            IsBusy = true;
            SelectedItem = null;
        }

        public Item SelectedItem
        {
            get => _selectedItem;
            set
            {
                SetProperty(ref _selectedItem, value);
                //OnItemSelected(value);
            }
        }

        private async void OnAddItem(object obj)
        {
            await Shell.Current.GoToAsync(nameof(NewItemPage));
        }

        async void OnItemSelected(EmployEnd employe)
        {
            if (employe == null)
                return;

            // This will push the ItemDetailPage onto the navigation stack
            await Shell.Current.GoToAsync($"{nameof(ItemDetailPage)}?{nameof(ItemDetailViewModel.Id)}={employe.Id} ");
        }
    }
}