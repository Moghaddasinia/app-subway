﻿using System.ComponentModel;
using Xamarin.Forms;
using SubWay.ViewModels;
using SubWay.Services;
using System;
using SubWay.Database;

namespace SubWay.Views
{
    public partial class ItemDetailPage : ContentPage
    {
        public ItemDetailPage()
        {
            InitializeComponent();
          
            BindingContext = new ItemDetailViewModel();

        }
        protected async override void OnAppearing()
        {
            base.OnAppearing();

            var employes = await InfoService.GetAllData();
            BindingContext = employes;
        }
        private void Avaliar(object sender, EventArgs e)
        {
            DbContext db = new DbContext();

            var nameFood = ((Button)sender).CommandParameter;
            db.AddOrEditTransaction(new Transaction()
            {
                name = (string)nameFood,
                count = 1
            });
            DisplayAlert("Add Food", (string)nameFood, "Ok");
        }

    }
}