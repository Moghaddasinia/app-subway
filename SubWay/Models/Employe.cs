﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SubWay.Models
{
    public class Employe
    {
        public string address { get; set; }
        public string name { get; set; }
        public string owner { get; set; }
        public IList<string> Items { get; set; }
        public EmployEnd[] emloyees { get; set; }
        public Items[] ites { get; set; }

    }

    public class EmployEnd
    {
        public int Id { get; set; }
        public string name { get; set; }
    }
    public class Items
    {
        public int id { get; set; }
        public string name { get; set; }
        public string image { get; set; }
        public Uri ImageUri
        {
            get { return string.IsNullOrEmpty(image) ? null : new Uri(image); }
        }
    }
}
