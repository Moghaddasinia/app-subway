﻿using Newtonsoft.Json;
using SubWay.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SubWay.Services
{
    public class InfoService
    {
        private const string WISHLIST_FILE = "wishlist.json";
        private static HttpClient client;
        private static object jsonconvert;

        public static List<Employe> WishList { get; set; }
        static InfoService()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://placehold.it/");
            WishList = new List<Employe>();

        }
        public static async Task<List<Employe>> DeserializeNames()
        {
            var employeRaw = await client.GetStringAsync("Products/");
            var serializer = new JsonSerializer();

            using (var tReader = new StringReader(employeRaw))
            {
                using (var jReader = new JsonTextReader(tReader))
                {
                    var employes = serializer.Deserialize<List<Employe>>(jReader);
                    return employes;
                }
            }
        }
        public static async Task<Employe> GetAllData()
        {
            string json = @"{
                    'name': 'Subway Zehlendorf',
                    'address': 'Potsdamer str. 11, 14163',
                    'owner': 'Amir Koklan',
                    'picture': 'http://placehold.it/32x32',
                    'emloyees': [
                      {
                                'id': 0,
                        'name': 'Whitney Sexton'
                      },
                      {
                                 'id': 1,
                        'name': 'Benton Morrison'
                      },
                      {
                                 'id': 2,
                        'name':'Alberta Golden'
                      },
                      {
                                'id': 3,
                        'name': 'Celeste Jensen'
                      }
                    ],
               'ites': [
                      {
                                'id': 0,
                        'name': 'Tomato',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 3
                      },
                      {
                                'id': 1,
                        'name': 'Salat',
                        'image': 'https://my.morrisons.com/globalassets/blogs/food/2018/june/32-ounce-steak/m-32-steak.jpg',
                        'expiry': 2
                      },
                      {
                               'id': 2,
                        'name': 'Salat',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 5
                      },
                      {
                                'id': 3,
                        'name': 'Tomato',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 3
                      },
                      {
                                'id': 4,
                        'name': 'Tomato',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 4
                      },
                      {
                                'id': 5,
                        'name': 'Tomato',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 1
                      },
                      {
                             'id': 6,
                        'name': 'Tomato',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 4
                      },
                      {
                                'id': 7,
                        'name': 'chicken',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 1
                      },
                      {
                               'id': 8,
                        'name': 'meat',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 3
                      },
                      {
                                'id': 9,
                        'name': 'chicken',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 4
                      },
                      {
                                'id': 10,
                        'name': 'Cucumber',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 1
                      },
                      {
                                'id': 11,
                        'name': 'Salat',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 5
                      },
                      {
                                'id': 12,
                        'name': 'Tomato',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 5
                      },
                      {
                                'id': 13,
                        'name': 'Cucumber',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 1
                      },
                      {
                                'id': 14,
                        'name': 'Cucumber',
                        'image': 'https://upload.wikimedia.org/wikipedia/commons/a/a8/Salad_platter02_crop.jpg',
                        'expiry': 4
                      }
                    ]
                   
                  }
                ";

            var employe = JsonConvert.DeserializeObject<Employe>(json);

            return employe;
        }
        public static async Task SaveWishList()
        {
            if (WishList != null)
            {
                var path = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                    WISHLIST_FILE);

                using (var swriter = new StreamWriter(path))
                {
                    using (var jwriter = new JsonTextWriter(swriter))
                    {
                        JsonSerializer.CreateDefault().Serialize(jwriter, WishList);
                    }
                }
            }
        }
        public static async Task LoadWishList()
        {
            var path = System.IO.Path.Combine(System.Environment.GetFolderPath(Environment.SpecialFolder.Personal),
                WISHLIST_FILE);
            if (File.Exists(path))
            {
                using (var reader = new StreamReader(path))
                {
                    using (var json = new JsonTextReader(reader))
                    {
                        try
                        {
                            WishList = JsonSerializer.CreateDefault().Deserialize<List<Employe>>(json);
                        }
                        catch (Exception e)
                        {
                            System.Diagnostics.Debug.WriteLine(e.Message);
                            throw;
                        }
                    }
                }
            }
        }
    }
}
