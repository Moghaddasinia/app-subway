﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using SubWay.Droid.Servieces;
using SubWay.Helpers;
using Environment = System.Environment;

[assembly: Xamarin.Forms.Dependency(typeof(FileHelper))]
namespace SubWay.Droid.Servieces
{
   public class FileHelper : IFileHelper
    {
        public string GetDbFilePath(string dbname)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            return Path.Combine(path, dbname);
        }
    }
}