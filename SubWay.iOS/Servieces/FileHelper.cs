﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using Foundation;
using SubWay.Helpers;
using SubWay.iOS.Servieces;
using UIKit;
[assembly: Xamarin.Forms.Dependency(typeof(FileHelper))]
namespace SubWay.iOS.Servieces
{
    public class FileHelper : IFileHelper
    {
        public string GetDbFilePath(string dbname)
        {
            string path = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            string libPath = Path.Combine(path, "..", "Library", "Databases");
            if (!Directory.Exists(libPath))
                Directory.CreateDirectory(libPath);

            return Path.Combine(libPath, dbname);
        }
    }
}